var express = require('express');
var app = express();

var dataFile = require('../data/data.json');



app.get("/", function(req, res){
    res.send("<h3>Express Connection success...</h3>" + req.url);
});

// api to get data based on employee id
app.get("/employees/:empId", function(req, res){
    var info='';
    dataFile.employees.forEach(function(item){
        if(item.id == req.params.empId){
            info += `
                <h3> ${item.id} </h3>
                <h3> ${item.name} </h3> 
                <h3> ${item.salary} </h3>
             `
        }});
        res.send(info);
    });    
    
// api too get data based on index
app.get("/employees/id/:index", function(req, res){
     var employee = dataFile.employees[req.params.index];
      
     res.send(`
     <h3> ${employee.id} </h3>
     <h3> ${employee.name} </h3>
     <h3> ${employee.salary} </h3>
     `);
    
});


app.server = app.listen(3000, function(){
    console.log("Server started on Port 3000...");
});
