let express=require("express");
let bodyparser=require("body-parser");
let app=express();
let cors = require('cors');

port = 3000;
let multer = require('multer');
let storage = multer.diskStorage({
    destination: './uploads',
    filename: function(req, file, cb){
        cb(null, file.originalname);
    }
})


const upload = multer({storage: storage})
app.use(cors());

app.use(bodyparser.json());
app.use("/uploads", express.static("uploads"));
app.use(bodyparser.urlencoded({extended:false}));
app.use("/productRegister", require("./productRegister/productRegister"));
app.use("/login",require("./login/login"));
app.use("/register",require("./register/register"));
app.use("/update",require("./update/update"));
app.use("/delete",require("./delete/delete"));
app.use("/fetch",require("./fetch/fetch"));
app.use("/productFetch",require("./productFetch/productFetch"));
app.use("/VegFetch",require("./VegFetch/VegFetch"));
app.use("/FruitFetch",require("./FruitFetch/FruitFetch"));
app.use("/GrainFetch",require("./GrainFetch/GrainFetch"));
app.use("/FarmWastesFetch",require("./FarmWastesFetch/FarmWastesFetch"));
app.post('/', upload.single('file'), (req, res)=>{})
app.listen(port);
console.log("server listening the port no 3000");