var express = require('express');
var bodyparser = require('body-parser');

let mongodb = require("mongodb");
let talentsprint = mongodb.MongoClient;

let fetch = express.Router().get("/", (req, res)=>{
    console.log('fetch started....');
     talentsprint.connect("mongodb://localhost:27017/freshFromFarmDb", (err, db)=>{
         if(err){
             throw err;
         }
         else{
             db.collection("FetchProduct").find({}).toArray((err, array)=>{
                 if(err){
                     throw err;
                 }
                 else{
                     if(array.length > 0){
                         console.log("in fetch js");
                         console.log("array");
                         console.log(array[array.length-1]['category']);
                         
                         res.send(array);
                     }
                     else{
                         res.send({message:"Record Not Found..."});
                     }
                 }
             });
         }
     });
});

module.exports = fetch;