import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddToCartService } from '../add-to-cart.service';
import { CustomerService } from '../customer.service';
import { ProductService } from '../product.service';
@Component({
  selector: 'app-show-product',
  templateUrl: './show-product.component.html',
  styleUrls: ['./show-product.component.css']
}) 
export class ShowProductComponent implements OnInit {
  products : any;
  
  constructor(private router : Router, private cService : CustomerService, private service: ProductService, private cartService : AddToCartService) { }
  
  ngOnInit(): void {
    this.service.fetchProduct().subscribe((result:any)=>{
      console.log(result);
      this.products=result;
    });
  } 

  addtocart(item : any){
    this.cartService.addtoCart(item);
    console.log("Hi on add to cart ts");
  }
}