import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginId: string;
  password:string;
  customer : any;
  flag: any;
  constructor(private router: Router, private service: CustomerService) { 
    this.loginId = '';
    this.password = '';
  }


  ngOnInit(): void {
  }

  loginSubmit():void{
    console.log('Login ID : '+ this.loginId);
    console.log('Password : ' + this.password);


    if(this.loginId === 'HR' && this.password === 'HR'){
      alert("Welcome to HR homepage");
    }
    else{
      this.flag = 0;
    this.customer.forEach((element: any)=>{
      if(element.email == this.loginId && element.password == this.password){
        console.log(element.email)
        alert('Welcome to employee Home Page');
        this.flag = 1;
      }
      if(this.flag == 0){
        alert("Invalid login");
      }
    })
  }
}
async loginSubmit2(loginForm: any){
    if(loginForm.loginId === 'Admin' && loginForm.password === 'admin1'){
      this.service.setUserLoggedIn();
      this.router.navigate(['adminpage']);
    }
    else{
      await this.service.getCustomerByEmailAndPassword(loginForm).then((result: any)=>{
       if(result != null){
        this.service.setUserLoggedIn();
        this.service.uPhno = result.phnNo;
        console.log("Phone No :", this.service.uPhno)
        this.router.navigate(['show-product']);
       // console.log(result);
       }
       else{
        alert('Check your credentials');
      }
      });

      }
    }
  }