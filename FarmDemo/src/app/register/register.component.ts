import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  cust : any;
  customer : any;
  constructor(private router: Router, private httpClient: HttpClient, private service: CustomerService) { 
    this.customer = {cName:'', phnNo:'', email:'', password:''};
   }

  ngOnInit(): void {
  }
  registerCust1(regForm: any){
     this.service.registerCustomer(regForm).subscribe();
     this.router.navigate(['login']);
  } 
}