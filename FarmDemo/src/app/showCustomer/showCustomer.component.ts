import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-showCustomer',
  templateUrl: './showCustomer.component.html',
  styleUrls: ['./showCustomer.component.css']
})
export class ShowCustomerComponent implements OnInit {
  customers:any;
  custdetails : any;
  add= false;
  constructor(private service: CustomerService) { 
  }
 
  ngOnInit(): void { 
      this.service.fetchDetails().subscribe((result:any)=>{
      this.customers = result;
    })
  }
  
  onAdd():void{
    this.add = true;
    
  }

  onRemove():void{
    this.add = false;
  }
  deleteCustomer(customer:any){
    this.service.deleteCustomer(customer).subscribe((result:any)=>{
      console.log(result);
      const i = this.customers.findIndex((element:any)=>{
        return customer.email === element.email;
      })
      this.customers.splice(i, 1);
    });
  }
 }