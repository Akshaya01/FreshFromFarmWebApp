import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RoundOffsets } from '@popperjs/core/lib/modifiers/computeStyles';
import { AddToCartService } from '../add-to-cart.service';

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.css']
})
export class OrderPageComponent implements OnInit {
  products : any;
  totalMoney : any;
  constructor(private cartService : AddToCartService, private router : Router) {
    
   }

  ngOnInit(): void {
    this.products = this.cartService.orderProducts;
    console.log("inside order page");
    console.log(this.products);
    this.totalMoney = this.cartService.getTotalPrice();
  }
  proceedToPay(){
    this.router.navigate(['pay']);
  }
}
