import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddToCartService } from '../add-to-cart.service';
import { CustomerService } from '../customer.service';
import { ProductService } from '../product.service';
@Component({
  selector: 'app-vegetables',
  templateUrl: './vegetables.component.html',
  styleUrls: ['./vegetables.component.css']
})
export class VegetablesComponent implements OnInit {
  vegetables: any;
  constructor(private router : Router, private cService : CustomerService, private service: ProductService, private cartService : AddToCartService) { }
  
  ngOnInit(): void {
    this.service.fetchVegetables().subscribe((result:any)=>{
      console.log(result);
      this.vegetables=result;
    });
  }

  

  addtocart(item : any){
    this.cartService.addtoCart(item);
  }
}
 