import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ProductService } from '../product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-product',
  templateUrl: './register-product.component.html',
  styleUrls: ['./register-product.component.css']
})
export class RegisterProductComponent implements OnInit {
  @ViewChild('fileInput', {static: false}) fileInput:ElementRef | undefined;
  product : any;
  imageName: any = "http://localhost:3000/uploads/" ;
  constructor(private httpClient: HttpClient, private service: ProductService, private router: Router) { 
    this.product = {pName:'', price:'', description:'', pImage: '', category:''};
   }
   
  ngOnInit(): void {
  }

  registerProd1(regForm: any){
    console.log(regForm)
    this.service.productRegister(regForm).subscribe();
    regForm['pImage'] = this.imageName;
    console.log(regForm)
    
 }
 onSelect(event : any){
    console.log("Image name: ");
    this.imageName = this.imageName + event.target.files[0].name;
    console.log(this.imageName);
 }
 onFileUpload(regForm: any){
   
   const imageBlob = this.fileInput?.nativeElement.files[0];
   const file = new FormData();
   file.set('file', imageBlob);
   regForm['pImage'] = this.imageName;
   this.service.productRegister(regForm).subscribe();
    
    this.httpClient.post('http://localhost:3000/', file).subscribe((response : any)=>{
      console.log(response);
    })
    this.router.navigate(['show-product']);
 }
}