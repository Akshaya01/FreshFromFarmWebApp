import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ProductService {
  constructor(private httpClient: HttpClient) { }
  fetchProduct(){
    return this.httpClient.get('http://localhost:3000/productFetch');
  }
  productRegister(product : any){
    return this.httpClient.post('http://localhost:3000/productRegister', product);
  }
  fetchVegetables(){
    return this.httpClient.get('http://localhost:3000/vegFetch');
  }
  fetchFruits(){
    return this.httpClient.get('http://localhost:3000/FruitFetch');
  }
  fetchFarmWastes(){
    return this.httpClient.get('http://localhost:3000/FarmWastesFetch');

  }
  fetchGrains(){
    return this.httpClient.get('http://localhost:3000/GrainFetch');

  }
}


