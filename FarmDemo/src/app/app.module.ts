import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { GooglePayButtonModule } from '@google-pay/button-angular';
import { AppComponent } from './app.component';
import { Demo1Component } from './demo1/demo1.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ShowCustomerComponent } from './showCustomer/showCustomer.component';
import { ExpPipe } from './exp.pipe';
import { GenderPipe } from './gender.pipe';
import { CountryCodePipe } from './country-code.pipe';
import { ProductComponent } from './product/product.component';
import { HeaderComponent } from './header/header.component';
import { ParentComponent } from './parent/parent.component';
import { PayComponent } from './pay/pay.component';
import { ChildComponent } from './child/child.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { adminpageComponent } from './adminpage/adminpage.component';
import { HttpClientModule } from '@angular/common/http';
import { FooterComponent } from './footer/footer.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { RegisterProductComponent } from './register-product/register-product.component';
import { ShowProductComponent } from './show-product/show-product.component';
import { CartComponent } from './cart/cart.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { MainComponent } from './main/main.component';
import { VegetablesComponent } from './vegetables/vegetables.component';
import { FruitsComponent } from './fruits/fruits.component';
import { GrainsComponent } from './grains/grains.component';
import { FarmwastesComponent } from './farmwastes/farmwastes.component';
import { OrderPageComponent } from './order-page/order-page.component';
import { LearnToFarmComponent } from './learn-to-farm/learn-to-farm.component';

@NgModule({
  declarations: [
    MainComponent,
    AppComponent,
    Demo1Component,
    LoginComponent,
    RegisterComponent,
    ShowCustomerComponent,
    ExpPipe,
    GenderPipe,
    HeaderComponent,
    CountryCodePipe,
    ProductComponent,
    PayComponent,
    ParentComponent,
    ChildComponent,
    PagenotfoundComponent,
    adminpageComponent,
    FooterComponent,
    AboutUsComponent,
    RegisterProductComponent,
    ShowProductComponent,
    CartComponent,
    PayComponent,
    MainHeaderComponent,
    VegetablesComponent,
    FruitsComponent,
    GrainsComponent,
    FarmwastesComponent,
    OrderPageComponent,
    LearnToFarmComponent
    ],
  imports: [
    BrowserModule,
    FormsModule, 
    GooglePayButtonModule,
    AppRoutingModule, 
    HttpClientModule
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }