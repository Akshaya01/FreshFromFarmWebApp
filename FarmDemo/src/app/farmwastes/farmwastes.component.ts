import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddToCartService } from '../add-to-cart.service';
import { CustomerService } from '../customer.service';
import { ProductService } from '../product.service';
 

@Component({
  selector: 'app-farmwastes',
  templateUrl: './farmwastes.component.html',
  styleUrls: ['./farmwastes.component.css']
})
export class FarmwastesComponent implements OnInit {

  farmwastes: any;
  constructor(private router : Router, private cService : CustomerService, private service: ProductService, private cartService : AddToCartService) { }
   
  ngOnInit(): void {
    this.service.fetchFarmWastes().subscribe((result:any)=>{
      console.log(result);
      this.farmwastes=result;
    });
  }

  

  addtocart(item : any){
    this.cartService.addtoCart(item);
  }
}
 