import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddToCartService } from '../add-to-cart.service';
import { CustomerService } from '../customer.service';
import { ProductService } from '../product.service';
 
@Component({
  selector: 'app-fruits',
  templateUrl: './fruits.component.html',
  styleUrls: ['./fruits.component.css']
})
export class FruitsComponent implements OnInit {
  fruits: any;
  constructor(private router : Router, private cService : CustomerService, private service: ProductService, private cartService : AddToCartService) { }
    
  ngOnInit(): void {
    this.service.fetchFruits().subscribe((result:any)=>{
      console.log(result);
      this.fruits=result;
    });
  }


  

  addtocart(item : any){
    this.cartService.addtoCart(item);
  }
}
 