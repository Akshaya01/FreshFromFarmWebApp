import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { adminpageComponent } from './adminpage/adminpage.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { LoginComponent } from './login/login.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { ProductComponent } from './product/product.component';
import { RegisterComponent } from './register/register.component';
import { RegisterProductComponent } from './register-product/register-product.component'; 
import { ShowCustomerComponent } from './showCustomer/showCustomer.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { ShowProductComponent } from './show-product/show-product.component';
import { CartComponent } from './cart/cart.component';
import { FooterComponent } from './footer/footer.component';
import { PayComponent } from './pay/pay.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { VegetablesComponent } from './vegetables/vegetables.component';
import { FruitsComponent } from './fruits/fruits.component';
import { GrainsComponent } from './grains/grains.component';
import { FarmwastesComponent } from './farmwastes/farmwastes.component';
import { LearnToFarmComponent } from './learn-to-farm/learn-to-farm.component';
import { OrderPageComponent } from './order-page/order-page.component';

const routes: Routes = [
  {path: '', component: MainComponent},
  {path: 'login', component: LoginComponent},
  {path: 'main', component: MainComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'register-product', component: RegisterProductComponent},
  {path: 'show-product', component: ShowProductComponent},
  {path: 'showCustomer', component: ShowCustomerComponent},
  {path: 'about-us', component: AboutUsComponent},
  {path: 'product', component: ProductComponent},
  {path: 'cart', component: CartComponent},
  {path: 'vegetables', component: VegetablesComponent},
  {path: 'fruits', component: FruitsComponent},
  {path: 'grains', component: GrainsComponent},
  {path: 'farmwastes', component: FarmwastesComponent},
  {path: 'order-page', component: OrderPageComponent},
  {path: 'learn-to-farm', component: LearnToFarmComponent},
  {path: 'pay', component: PayComponent},
  {path: 'header',
    children:[
      {path: '', component: HeaderComponent},
      {path: 'login', canActivate: [AuthGuard], component: LoginComponent},
      {path: 'main', canActivate: [AuthGuard], component: MainComponent},
      {path: 'show-product', canActivate: [AuthGuard], component: ShowProductComponent},
      {path: 'register', canActivate: [AuthGuard], component: RegisterComponent},
      {path: 'learn-to-farm', canActivate: [AuthGuard], component: LearnToFarmComponent},
    ]},
    {path: 'main',
    children:[
      {path: '', component: LoginComponent},
      {path: 'login', canActivate: [AuthGuard], component: LoginComponent},
      {path: 'main', canActivate: [AuthGuard], component: MainComponent},
      {path: 'register', canActivate: [AuthGuard], component: RegisterComponent},
    ]},
    {path: 'login',
    children:[
      {path: '', component: LoginComponent},
      {path: 'main', canActivate: [AuthGuard], component: MainComponent},
      {path: 'showCustomer', canActivate: [AuthGuard], component: ShowCustomerComponent},
      {path: 'show-product', canActivate: [AuthGuard], component: ShowProductComponent},
      {path: 'adminpage', canActivate: [AuthGuard], component: adminpageComponent},
    ]},
  {path: 'adminpage',
    children:[
      {path: '', component: adminpageComponent},
      {path: 'showCustomer', canActivate: [AuthGuard], component: ShowCustomerComponent},
      {path: 'show-product', canActivate: [AuthGuard], component: ShowProductComponent},
      {path: 'register-product', canActivate: [AuthGuard], component: RegisterProductComponent},
    ]},
    {path: 'footer',
    children:[
      {path: '', component: FooterComponent},
      {path: 'about-us', canActivate: [AuthGuard], component: AboutUsComponent},
    ]},
    {path: 'show-product',
    children:[
      {path: '', component: ShowProductComponent},
      {path: 'cart', canActivate: [AuthGuard], component: CartComponent},
      {path: 'vegetables', canActivate: [AuthGuard], component: VegetablesComponent},
      {path: 'fruits', canActivate: [AuthGuard], component: FruitsComponent},
      {path: 'grains', canActivate: [AuthGuard], component: GrainsComponent},
      {path: 'farmwastes', canActivate: [AuthGuard], component: FarmwastesComponent},
    ]},
    {path: 'vegetables',
    children:[
      {path: '', component: VegetablesComponent},
      {path: 'fruits', canActivate: [AuthGuard], component: FruitsComponent},
      {path: 'grains', canActivate: [AuthGuard], component: GrainsComponent},
      {path: 'farmwastes', canActivate: [AuthGuard], component: FarmwastesComponent},
    ]},
    {path: 'fruits',
    children:[
      {path: '', component: FruitsComponent},
      {path: 'vegetables', canActivate: [AuthGuard], component: VegetablesComponent},
      {path: 'grains', canActivate: [AuthGuard], component: GrainsComponent},
      {path: 'farmwastes', canActivate: [AuthGuard], component: FarmwastesComponent},
    ]},
    {path: 'grains',
    children:[
      {path: '', component: GrainsComponent},
      {path: 'vegetables', canActivate: [AuthGuard], component: VegetablesComponent},
      {path: 'fruits', canActivate: [AuthGuard], component: FruitsComponent},
      {path: 'farmwastes', canActivate: [AuthGuard], component: FarmwastesComponent},
    ]},
    {path: 'farmwastes',
    children:[
      {path: '', component: FarmwastesComponent},
      {path: 'vegetables', canActivate: [AuthGuard], component: VegetablesComponent},
      {path: 'grains', canActivate: [AuthGuard], component: GrainsComponent},
      {path: 'fruits', canActivate: [AuthGuard], component: FruitsComponent},
    ]},
    {path: 'cart',
    children:[
      {path: '', component: CartComponent},
      {path: 'show-product', canActivate: [AuthGuard], component: CartComponent},
      {path: 'order-page', canActivate: [AuthGuard], component: OrderPageComponent}
    ]},
    {path: 'about-us',
    children:[
      {path: '', component: AboutUsComponent},
      {path: 'show-product', canActivate: [AuthGuard], component: ShowProductComponent},
    ]},
    {path: 'order-page',
    children:[
      {path: '', component: OrderPageComponent},
      {path: 'pay', canActivate: [AuthGuard], component: PayComponent},
    ]},
    {path: 'register-product',
    children:[
      {path: '', component: RegisterProductComponent},
      {path: 'show-product', canActivate: [AuthGuard], component: ShowProductComponent},
    ]},
  {path: '**', component: PagenotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
