import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo1',
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.css']
})
export class Demo1Component implements OnInit {

  message : string ;
  name: string;
  age: number;
  isMarried: boolean;
  salary : number;
  hobbies : any;
  address: any;

  constructor() {
    // it will automatically invoke when an object is created
    // used for initializing class variables
    // dependency injection (this is use in angular)
    // don't write the code here
    //alert("constructor is executed");
    this.message = "Hello World";
    this.name = "Akshaya TL";
    this.age = 19;
    this.isMarried = false;
    this.salary = 100000000;
    this.hobbies = ['Playing', 'Chatting', 'Sleeping', 'Eating', 'Reading', 'Exercise'];
    this.address = {doorNo:101, street: "Gachibowli", city:"Hyderabad"};
   }

   //for services
  ngOnInit(): void {
    //it invokes when the component is created
    //write any init code of angular
    //alert("ngOnInit is executed");
    
  } 
  showMessage() : void{
   alert('in showMessage() method')   
  }

}
