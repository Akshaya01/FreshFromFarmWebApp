import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddToCartService } from '../add-to-cart.service';
import { CustomerService } from '../customer.service';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-grains',
  templateUrl: './grains.component.html',
  styleUrls: ['./grains.component.css']
})
export class GrainsComponent implements OnInit {
  
  grains: any;
  constructor(private router : Router, private cService : CustomerService, private service: ProductService, private cartService : AddToCartService) { }
  
  ngOnInit(): void {
    this.service.fetchGrains().subscribe((result:any)=>{
      console.log(result);
      this.grains=result;
    });
  }

  

  addtocart(item : any){
    this.cartService.addtoCart(item);
  }
}
 