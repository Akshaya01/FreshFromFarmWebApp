import { Component, OnInit } from '@angular/core';
import { AddToCartService } from '../add-to-cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  products : any;
  counter : number = 0;
  public filterCategory : any
  searchText:string ="";

  constructor() {
    this.products = [
      {ProductId:1, ProductName:'Banana',                ProductPrice:29.95,  Quantity:"1 kg",         description:'good source of potassium, fiber and vitamin C',       imagePath:"/assets/images/banana.jpg",                       alt:"Fruit"},
      {ProductId:2, ProductName:'Apple',                 ProductPrice:140,    Quantity:"4 Units",      description:'rich in fiber and antioxidants',                      imagePath:"assets/images/apple.jpg",                         alt:"Fruit"},
      {ProductId:3, ProductName:'Onion',                 ProductPrice:35.35,  Quantity:"1 kg",         description:'packed with nutrients',                               imagePath:"assets/images/onion.jpg",                         alt:"Vegetable"},
      {ProductId:4, ProductName:'Pineapple',             ProductPrice:70,     Quantity:"1 Unit",       description:'good source of antioxidants',                         imagePath:"/assets/images/pineapple.jpg",                    alt:"Fruit"},
      {ProductId:5, ProductName:'Chopped Straw',         ProductPrice:29.95,  Quantity:"1 kg",         description:'chopped paddy straw',                                 imagePath:"assets/images/chopped straw (1).jpg",             alt:"Farm Waste"},
      {ProductId:6, ProductName:'Cotton White Yarn',     ProductPrice:75,     Quantity:"1 kg",         description:'Cotton Yarn',                                         imagePath:"assets/images/cotton white yarn waste 75kg.jpg",  alt:"Farm Waste"},
      {ProductId:7, ProductName:'Paddy Straw',           ProductPrice:35.35,  Quantity:"1 kg",         description:'paddy straw',                                         imagePath:"assets/images/paddy straw.jpg",                   alt:"Farm Waste"},
      {ProductId:8, ProductName:'Compost Leaves',        ProductPrice:70,     Quantity:"1 kg",         description:'compost made of dried leaves',                        imagePath:"assets/images/compost leaves.jpeg",               alt:"Farm Waste"},
      {ProductId:9, ProductName:'Barley',                ProductPrice:14.14,  Quantity:"500 gms",      description:'Barley grains',                                       imagePath:"/assets/images/barley.png",                       alt:"Grains"},
      {ProductId:10, ProductName:'Rice',                 ProductPrice:54,     Quantity:"1 kg",         description:'Rice grains',                                         imagePath:"/assets/images/rice.png",                         alt:"Grains"},
      {ProductId:11, ProductName:'Vegetable Compost',    ProductPrice:21,     Quantity:"1 kg",         description:'compost made from fruit and vegetable wastes',        imagePath:"/assets/images/vegetable compost.jpg",            alt:"Farm Waste"},
      {ProductId:12, ProductName:'Carrot',               ProductPrice:14.14,  Quantity:"500 gms",      description:'good source of vitamin A and beta-carotene',          imagePath:"/assets/images/carrot.jpg",                       alt:"Vegetable"},
      {ProductId:13, ProductName:'Mango',                ProductPrice:105,    Quantity:"1 kg",         description:'packed with magnesium and potassium',                 imagePath:"/assets/images/mango.jpg",                        alt:"Fruit"},
      {ProductId:14, ProductName:'Potato',               ProductPrice:21,     Quantity:"1 kg",         description:'rich in fiber and antioxidants',                      imagePath:"/assets/images/potato.jpg",                       alt:"Vegetable"},
      {ProductId:15, ProductName:'Tomato',               ProductPrice:19,     Quantity:"1 kg",         description:'good source of potassium, vitamin B and E',           imagePath:"/assets/images/tomato.jpg",                       alt:"Vegetable"},
      {ProductId:16, ProductName:'Orange',               ProductPrice:179,    Quantity:"6 Units",      description:'good source of dietary fiber and vitamin C',          imagePath:"/assets/images/orange.jpg",                       alt:"Fruit"},
      {ProductId:17, ProductName:'Avacado',              ProductPrice:356.25, Quantity:"2 Units",      description:'good source of potassium, fiber',                     imagePath:"/assets/images/avacado.jpg",                      alt:"Fruit"},
      {ProductId:18, ProductName:'Basmati Rice',         ProductPrice:130,    Quantity:"1 kg",         description:'good source of fiber',                                imagePath:"assets/images/basmathi_rice.webp",                alt:"Grains"},
      {ProductId:19, ProductName:'Brown Rice',           ProductPrice:149,    Quantity:"1 kg",         description:'rich source of dietry fiber',                         imagePath:"assets/images/brown_rice.png",                    alt:"Grains"},
      {ProductId:20, ProductName:'Cherry',               ProductPrice:375,    Quantity:"100 gms",      description:'good source of fiber, vitamins, minerals',            imagePath:"/assets/images/cherry.webp",                      alt:"Fruit"},
      {ProductId:21, ProductName:'Corn',                 ProductPrice:35,     Quantity:"2 Units",      description:'rich in vitamin C and antioxidants',                  imagePath:"assets/images/corn.jpg",                          alt:"Grains"},
      {ProductId:22, ProductName:'Green Grapes',         ProductPrice:45,     Quantity:"500 gms",      description:'rich in vitamin C and K',                             imagePath:"assets/images/grapes green.jpg",                  alt:"Fruit"},
      {ProductId:23, ProductName:'Black Grapes',         ProductPrice:55,     Quantity:"500 gms",      description:'rich in vitamin C and K',                             imagePath:"assets/images/grapes.jpg",                        alt:"Fruit"},
      {ProductId:24, ProductName:'Green Moong Dal',      ProductPrice:130,    Quantity:"1 kg",         description:'good source of dietry fiber',                         imagePath:"assets/images/green moong dal.webp",              alt:"Grains"},
      {ProductId:25, ProductName:'Jowar',                ProductPrice:70,     Quantity:"1 kg",         description:'good source of vitamin B',                            imagePath:"/assets/images/jowar.jpg",                        alt:"Grains"},
      {ProductId:26, ProductName:'Kiwi',                 ProductPrice:90,     Quantity:"3 Units",      description:'rich in vitamins and antioxidants',                   imagePath:"/assets/images/kiwi.jpg",                         alt:"Fruits"},
      {ProductId:27, ProductName:'Maize',                ProductPrice:75,     Quantity:"1 kg",         description:'good source of dietry fiber',                         imagePath:"/assets/images/maize.jpg",                        alt:"Grains"},
      {ProductId:28, ProductName:'Moong Dal',            ProductPrice:141,    Quantity:"1 kg",         description:'good source of fiber, vitamins',                      imagePath:"/assets/images/moongdal.jpg",                     alt:"Grains"},
      {ProductId:29, ProductName:'Muskmelon',            ProductPrice:37.50,  Quantity:"900 gms",      description:'rich in vitamin C and antioxidants',                  imagePath:"/assets/images/muskmelon.jpg",                    alt:"Fruit"},
      {ProductId:30, ProductName:'Rajma with Husk',      ProductPrice:103,    Quantity:"500 gms",      description:'rich in iron, potassium, vitamin K1',                 imagePath:"/assets/images/rajma with husk.jpg",              alt:"Grains"},
      {ProductId:31, ProductName:'Rajma without Husk',   ProductPrice:168,    Quantity:"1 kg",         description:'rich in iron, potassium, vitamin K1',                 imagePath:"/assets/images/rajma without husk.webp",          alt:"Grains"},
      {ProductId:32, ProductName:'Red And Green Grapes', ProductPrice:45,     Quantity:"500 gms",      description:'rich in vitamin C and K',                             imagePath:"/assets/images/red and green grapes.jpg",         alt:"Fruit"},
      {ProductId:33, ProductName:'Sapota',               ProductPrice:50,     Quantity:"500 gms",      description:'rich in vitamins and antioxidants',                   imagePath:"/assets/images/sapota.jpg",                       alt:"Fruit"},
      {ProductId:34, ProductName:'Tur Dal',              ProductPrice:110,    Quantity:"1 kg",         description:'rich in fiber, protein',                              imagePath:"/assets/images/tur dal.jpg",                      alt:"Grains"},
      {ProductId:35, ProductName:'Watermelon',           ProductPrice:50,     Quantity:"1.5 kg",       description:'rich in fiber',                                       imagePath:"/assets/images/watermelon.jpg",                   alt:"Fruit"},
      {ProductId:36, ProductName:'Beans',                ProductPrice:26,     Quantity:"500 gms",      description:'rich in vitamins and antioxidants',                   imagePath:"/assets/images/beans.jpg",                        alt:"Vegetable"},
      {ProductId:37, ProductName:'Round Brinjal',        ProductPrice:22.50,  Quantity:"900 gms",      description:'rich in antioxidants',                                imagePath:"/assets/images/fat brinjal.jpg",                  alt:"Vegetable"},
      {ProductId:38, ProductName:'Gongura',              ProductPrice:30,     Quantity:"250 gms",      description:'rich in vitamins',                                    imagePath:"/assets/images/gongura.jpg",                      alt:"Vegetable"},
      {ProductId:39, ProductName:'Green Chillies',       ProductPrice:30,     Quantity:"250 gms",      description:'rich in vitamins',                                    imagePath:"/assets/images/green chillies.jpg",               alt:"Vegetable"},
      {ProductId:40, ProductName:'Local Beans',          ProductPrice:12,     Quantity:"250 gms",      description:'rich in vitamins and antioxidants',                   imagePath:"/assets/images/local beans.jpg",                  alt:"Vegetable"},
      {ProductId:41, ProductName:'Long Brinjal',         ProductPrice:18,     Quantity:"500 gms",      description:'rich in antioxidants',                                imagePath:"/assets/images/long brinjal.jpg",                 alt:"Vegetable"},
      {ProductId:42, ProductName:'Spinach',              ProductPrice:72.50,  Quantity:"500 gms",      description:'rich in vitamins',                                    imagePath:"/assets/images/spinach.jpg",                      alt:"Vegetable"},
      {ProductId:43, ProductName:'Red Chillies',         ProductPrice:150,    Quantity:"1 kg",         description:'rich in vitamins',                                    imagePath:"/assets/images/red chillies.webp",                alt:"Vegetable"}
    ]
   }

  ngOnInit(): void {
  }

  onDec(){
    this.counter -= 1;
    return this.counter;
  }

  onInc(){
    this.counter += 1;
    return this.counter;
  }

  addToCart(product: any): void {
    console.log(product);
  }
}
