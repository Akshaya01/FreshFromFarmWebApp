import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  userLoggedIn: boolean;
  uPhno : any;
  constructor(private httpClient: HttpClient) { 
    this.userLoggedIn=false;    //initially set to false
  }
  // to transfer value of a private variable we create a getter setter method
  getUserLoggedIn():any{
    return this.userLoggedIn;   // invoked whenever we need this value 
  }
  setUserLoggedIn():any{
    this.userLoggedIn=true;     // invoked whenever we logged in successfully
  }
  
  setUserLoggedOut():any{
    this.userLoggedIn=false;    // invoked whenever we logged out
  }
  fetchDetails(){
    return this.httpClient.get('http://localhost:3000/fetch');
  }
  registerCustomer(customer : any){
    return this.httpClient.post('http://localhost:3000/register', customer);
  }
  getCustomerByEmailAndPassword(loginForm: any){
    return this.httpClient.get('http://localhost:3000/login/'+loginForm.email+"/"+loginForm.password).toPromise();
  }
  deleteCustomer(customer: any){
    return this.httpClient.delete('http://localhost:3000/delete/'+customer.email);
  }
  addCart(cart: any){
    console.log("here at c service");
      return this.httpClient.post('http://localhost:3000/addCart/', this.uPhno, cart);
  }
  
}
