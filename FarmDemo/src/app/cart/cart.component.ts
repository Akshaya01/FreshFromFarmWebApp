import { Component, OnInit } from '@angular/core';
import { AddToCartService } from '../add-to-cart.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  public productsLs : any = [];
  public grandTotal !: number;
  constructor(private cartService : AddToCartService, private router : Router) { }

  ngOnInit(): void {
      this.cartService.getProducts().subscribe(res=>{
      this.productsLs = res;
      this.grandTotal = this.cartService.getTotalPrice();
    })
    console.log(this.productsLs);
  }
  removeItem(item: any){
    this.cartService.removeCartItem(item);
  }
  emptycart(){
    this.cartService.removeAllCart();
  }
  goToOrderPage(){
    this.cartService.orderProducts = this.productsLs;
    console.log(this.cartService.orderProducts);
    this.router.navigate(['order-page']);
  }

}