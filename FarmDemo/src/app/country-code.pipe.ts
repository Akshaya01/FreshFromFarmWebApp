import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'countryCode'
})
export class CountryCodePipe implements PipeTransform {
  countryAndcodes : any;
  constructor(){
    this.countryAndcodes = [{cntry: "india", code: "+91 "}, {cntry:"usa", code: "+1 "}, {cntry:"germany", code: "+49"}, {cntry:"uk", code: "+91"}];
  }
  transform(value: any, country: any): any {
    for(let candc of this.countryAndcodes){
      if (candc.cntry === country.toLowerCase()) return (candc.code + value);
    }
    return value;
  }
}