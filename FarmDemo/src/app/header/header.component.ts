import { Component, OnInit } from '@angular/core';
import {Route, Router} from '@angular/router';
import { AddToCartService } from '../add-to-cart.service';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  loginUser : any;
  public totalItem : number = 0;
  constructor(private cService:CustomerService, private router : Router, private cartService : AddToCartService) {
    this.loginUser = this.cService.getUserLoggedIn();
    this.cartService.getProducts()
    .subscribe(res=>{
      this.totalItem = res.length;
    })
    }

  ngOnInit(): void {}

}
