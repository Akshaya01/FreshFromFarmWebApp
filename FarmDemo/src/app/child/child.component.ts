import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  @Input() c_empId : any;
  @Input() c_empName: any;
  @Input() c_salary: any;
  constructor() { }

  ngOnInit(): void {
  }
  @Output() send : EventEmitter<any> = new EventEmitter();
sendData(){
  // write code to send selected data to parent
  this.send.emit()
}
}
