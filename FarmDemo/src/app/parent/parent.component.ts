import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {
  employees : any;
  constructor() {  }

  ngOnInit(): void {
    this.employees = [
      {empname:"Akshaya TL", empId: "19wh1a0531", salary: 100000000},
      {empname:"Shreya T",   empId: "19wh1a1323", salary: 10000000},
      {empname:"Aditya R",   empId: "19wh1a2331", salary: 20000000},
      {empname:"Riddhi M",   empId: "19wh1a1341", salary: 10400000},
      {empname:"Crystal J",  empId: "19wh1a6456",  salary: 100000000},
    ];
  }

}
