# FreshFromFarm

Website for farmers, to sell there crops at reasonable prices.
##### Project has different views : 
                                - Admin view
                                - Customer view
##### Role of customer :
                    - Add to cart the required products
                    - Buy the products
                    - proceed for payments
##### Role of Admin :
                    - Administer the website 
                    - Update and delete the required details
                    - Set the details in the database

##### Tech Stack:
                - M : MongoDB for database 
                - E : Express JS for BackEnd Frame work
                - A : Angular for FrontEnd Frame work 
                - N : Node JS for runtime environment
                - Code-Editor : Visual studio code 
                - Version Control : GIT
                - Presentation : LaTex

######                          ENJOY FARMING!!!  ENJOY BUYING!!!
